#!/bin/bash
# /etc/init.d/minecraft
# version 1.0 2016-01-16 (YYYY-MM-DD)

### INFO
# Provides:   Minecraft Forge server
# Short-Description:    Forge server control script
# Description:    Controls Minecraft Forge server. Start, stop, kill, backup, etc...
### END INFO

# Settings
SERVICE='forge.jar'
MCPATH="/root"
BACKUPPATH="/home/backups"
CHECKSERVER="/root/CheckServer"
INVOCATION="java -server -Xmx1728M -XX:+AggressiveOpts -XX:+UseFastAccessorMethods -XX:+UseParallelGC -XX:SurvivorRatio=16 -Xnoclassgc -Xincgc -XX:-UseParallelGC -XX:+CITime -Djava.net.preferIPv4Stack=true -Dfml.queryResult=confirm -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+TieredCompilation -jar /root/forge.jar"
BACKUPARCHIVEPATH=$BACKUPPATH/archive
BACKUPDIR=$(date +%d_%b_%Y)
PORT=25565
if [ -z "$PORT" ]; then
	PORT=25565
fi
# Settings end

is_running() {
	if [ ! -e $MCPATH/java.pid ]; then
		return 1
	fi
	pid=$(cat $MCPATH/java.pid)
	if [ -z $pid ]; then
		return 1
	fi
	ps -eo "%p" | grep "^\\s*$pid\\s*\$" > /dev/null
	return $?
}

mc_start() {
	if is_running; then
		echo "Server already running"
	else
		echo "Server started"
		cd $MCPATH
		screen -dmS mc$PORT $INVOCATION &
		for (( i=0; i < 10; i++ )); do
			screenpid=$(ps -eo '%p %a' | grep -v grep | grep -i screen | grep mc$PORT | awk '{print $1}')
			javapid=$(ps -eo '%P %p' | grep "^\\s*$screenpid " | awk '{print $2}')
			if [[ -n "$screenpid" && -n "$javapid" ]]; then
				break
			fi
			sleep 1
		done
		if [[ -n "$screenpid" && -n "$javapid" ]]; then
			echo "$SERVICE is now running."
			echo "$javapid" > $MCPATH/java.pid
			echo "$screenpid.mc$PORT" > $MCPATH/screen.name
		else
			echo "Could not start $SERVICE."
		fi
	fi
}

mc_startmonitor() {
	if [ -z $CHECKSERVER ]; then
		echo "Started server monitor"
		/usr/bin/daemon --name=minecraft_checkserver -- $java -cp $CHECKSERVER CheckServer localhost $PORT
	fi
}

mc_saveoff() {
	if is_running; then
		echo "$SERVICE is running... suspending saves"
		mc_exec "say Server backup started. Server going read-only..."
		mc_exec "save-off"
		mc_exec "save-all"
		sync
		sleep 10
	else
		echo "$SERVICE was not running. Not suspending saves."
	fi
}

mc_saveon() {
	if is_running; then
		echo "$SERVICE is running... re-enabling saves"
		mc_exec "save-on"
		mc_exec "say Server backup complete. Server going read-write."
	else
		echo "$SERVICE was not running. Not resuming saves."
	fi
}

mc_kill() {
	pid=$(cat $MCPATH/java.pid)

	echo "terminating process with pid $pid"
	kill $pid
	for (( i=0; i < 10; i++ )); do
		is_running || break
		sleep 1
	done

	if is_running; then
		echo "$SERVICE could not be terminated, killing..."
		kill -SIGKILL $pid
		echo "$SERVICE killed"
	else
		echo "$SERVICE terminated"
	fi
}

mc_stop() {
	if is_running; then
		echo "$SERVICE is running... stopping."
		mc_exec "say Server will shutdown in 30 seconds. Saving map..."
		mc_exec "save-all"
		sleep 30
		mc_exec "stop"
		for (( i=0; i < 80; i++ )); do
			is_running || break
			sleep 1
		done
	else
		echo "$SERVICE was not running."
	fi
	if is_running; then
		echo "$SERVICE could not be shut down cleanly. Killing..."
		mc_kill
	else
		echo "$SERVICE is shut down."
	fi
	rm $MCPATH/java.pid
	rm $MCPATH/screen.name
}

mc_stopmonitor() {
	if [ -z $CHECKSERVER ]; then
		/usr/bin/daemon --name=minecraft_checkserver --stop
	fi
}

mc_backup() {
	echo "Backing up minecraft world"

	[ -d "$BACKUPPATH/$BACKUPDIR" ] || mkdir -p "$BACKUPPATH/$BACKUPDIR"

	rdiff-backup $MCPATH "$BACKUPPATH/$BACKUPDIR"
	echo "Backup complete"
}

mc_thinoutbackup() {
	if (($(date +%H) == 0)) && (($(date +%M) < 15)); then
		archivedate=$(date --date="7 days ago")
		echo "Thinning backups created $archivedate out"
		archivedateunix=$(date --date="$archivedate" +%s)
		archivesourcedir=$BACKUPPATH/$(date --date="$archivedate" +%b_%Y)
		archivesource=$archivesourcedir/rdiff-backup-data/increments.$(date --date="$archivedate" +%Y-%m-%dT%H):0*.dir
		archivesource=$(echo $archivesource)
		archivedest=$BACKUPARCHIVEPATH/$(date --date="$archivedate" +%b_%Y)
		if [[ ! -f $archivesource ]]; then
			echo "Nothing to be done"
		else
			tempdir=$(mktemp -d)
			if [[ ! $tempdir =~ ^/tmp ]]; then
				echo "invalid tmp dir $tempdir"
			else
				rdiff-backup $archivesource $tempdir
				rdiff-backup --current-time $archivedateunix $tempdir $archivedest
				rm -R "$tempdir"
				rdiff-backup --remove-older-than 7D --force $archivesourcedir
				echo "done"
			fi
		fi
	fi
}

mc_exec() {
	if is_running; then
		screen -p 0 -S $(cat $MCPATH/screen.name) -X stuff "$@$(printf \\r)"
	else
		echo "$SERVICE was not running. Not executing command."
	fi
}

# Listening for args here
case "$1" in
	start)
		if mc_start
		then
			mc_startmonitor
		fi
	;;
	stop)
		mc_stopmonitor
		mc_stop
    ;;
	restart)
		mc_stop
		mc_start
    ;;
	backup)
		mc_saveoff
		mc_backup
		mc_saveon
		mc_thinoutbackup
    ;;
	exec)
		shift
		mc_exec "$@"
    ;;
	status)
		if is_running
		then
			echo "$SERVICE is running."
		else
			echo "$SERVICE is not running."
		fi
    ;;
	say)
		shift
		echo "Sent to server: $@"
		mc_exec "say $@"
	;;
  *)
  echo "Usage: $(readlink -f $0) {start|stop|restart|backup|exec \"command\"|status|say \"message\"}"
  exit 1
  ;;
esac

exit 0
